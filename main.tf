terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
    region ="eu-west-2"
    access_key = "AKIA6FEPZD33P6YMAZ2O"
    secret_key = "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"
}

data "aws_ami" "petclinic_ami" {
  most_recent = true

  filter {
    name 	= "name"
    values 	= ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name	= "virtualization-type"
    values 	= ["hvm"]
  }
 
  owners = ["099720109477"]
}

resource "aws_instance" "petclinic_vm" {
    ami = data.aws_ami.petclinic_ami.id
    instance_type = "t3.micro"
    key_name = "javid"

    subnet_id = aws_subnet.petclinic_public_subnet.id
    security_groups = [aws_security_group.petclinic_security_group_public_subnet.id]

    tags = {
        Name = "petclinic_app"
    }

    user_data = <<EOF
        
        #!/bin/sh
        sudo apt update -y.
        sudo apt install docker.io -y
        sudo service docker start
        sudo docker pull javidjhuseyn/petclinic-app:latest
        sudo docker run -d -p 80:8080 -e MYSQL_USER=petclinic -e MYSQL_PASS=petclinic -e MYSQL_URL=jdbc:mysql://${aws_db_instance.petclinic_database.address}:3306/petclinic_db javidjhuseyn/petclinic-app
    EOF

}
