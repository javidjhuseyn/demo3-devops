resource "aws_vpc" "petclinic_vpc"{
    cidr_block = "10.0.0.0/16"

    tags = {
        Name = "petclinic_vpc"
    }
}

resource "aws_internet_gateway" "petclinic_internet_gateway" {
    vpc_id = aws_vpc.petclinic_vpc.id

    tags = {
        Name = "petclinic_internet_gateway"
    }
  
}

resource "aws_subnet" "petclinic_public_subnet"{
    vpc_id = aws_vpc.petclinic_vpc.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "eu-west-2a"
    map_public_ip_on_launch = true //true for public subnet

    tags = {
        Name = "petclinic_public_subnet"
    }

}

resource "aws_subnet" "petclinic_private_subnet" {
    vpc_id = aws_vpc.petclinic_vpc.id
    cidr_block = "10.0.2.0/24"
    availability_zone = "eu-west-2b"

    tags = {
        Name = "petclinic_private_subnet"
    }
  
}

resource "aws_subnet" "petclinic_private_subnet_reserved" {
    vpc_id = aws_vpc.petclinic_vpc.id
    cidr_block = "10.0.3.0/24"
    availability_zone = "eu-west-2c"

    tags = {
        Name = "petclinic_private_subnet_reserved"
    }
  
}

resource "aws_db_subnet_group" "petclinic_database_subnet_group" {
    subnet_ids = [aws_subnet.petclinic_private_subnet.id, aws_subnet.petclinic_private_subnet_reserved.id]

    tags = {
      Name = "petclinic_database_subnet_group"
    }
}

resource "aws_route" "route" {
  route_table_id         = aws_vpc.petclinic_vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.petclinic_internet_gateway.id
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.petclinic_vpc.id
}


resource "aws_route_table_association" "public_route_table_association" {
    subnet_id = aws_subnet.petclinic_public_subnet.id
    route_table_id = aws_vpc.petclinic_vpc.main_route_table_id
}

resource "aws_route_table_association" "private_route_table_association_1" {
  subnet_id      = aws_subnet.petclinic_private_subnet.id
  route_table_id = aws_route_table.private_route_table.id
}

resource "aws_route_table_association" "private_route_table_association_2" {
  subnet_id      = aws_subnet.petclinic_private_subnet_reserved.id
  route_table_id = aws_route_table.private_route_table.id
}


resource "aws_security_group" "petclinic_security_group_public_subnet" {
    vpc_id = aws_vpc.petclinic_vpc.id

    ingress {
        description  = "HTTP from VPC"
        from_port  = 80
        to_port  = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description  = "SSH for VPC"
        from_port  = 22
        to_port  = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress{
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
}

resource "aws_security_group" "petclinic_security_group_database" {
    vpc_id = aws_vpc.petclinic_vpc.id

    ingress {
        description  = "HTTP from VPC"
        from_port  = 3306
        to_port  = 3306
        protocol = "tcp"
        cidr_blocks = [aws_vpc.petclinic_vpc.cidr_block]
    }

    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
}
