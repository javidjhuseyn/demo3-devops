resource "aws_db_instance" "petclinic_database" {
  allocated_storage      = 20
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t2.small"
  name                   = "petclinic_db"
  username               = "petclinic"
  password               = "petclinic"
  storage_type           = "gp2"
  publicly_accessible    = false
  multi_az               = false
  max_allocated_storage  = 25
  skip_final_snapshot    = true
  identifier             = "petclinicdb"
  db_subnet_group_name   = aws_db_subnet_group.petclinic_database_subnet_group.name
  vpc_security_group_ids = [aws_security_group.petclinic_security_group_database.id]
}
